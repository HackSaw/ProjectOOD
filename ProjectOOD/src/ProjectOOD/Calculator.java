package ProjectOOD;

//Packages
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;


public class Calculator extends JFrame
{
	//Text Field (Like the screen on a physical calculator)
	private JTextField output = new JTextField();
	
	// create a linked list
    LinkedList <String> storage = new LinkedList();
	
	public Calculator(String title)
	{
		//Title, Window Layout, Window Colour 
		super(title);
		//set size of grid
		
		setBackground(Color.gray);
		//Set size of calculator window. Make it so that the user can't resize the window.
		setSize(350, 400);
		setResizable(false);
		
		//Add the textfeild to the top of the frame and specify it's size.
		JPanel panelN = new JPanel(); // FlowLayout used by default.
		output.setPreferredSize( new Dimension( 270, 50 ) );
		panelN.add(output);
		add(panelN, BorderLayout.NORTH);
		panelN.setVisible(true);
		
		
		// <<< BUTTON PANEL >>>
		JPanel panelB = new JPanel();
		panelB.setLayout(new GridLayout(4,3,7,10));

		
		// <<< BUTTON PANEL >>>
		JPanel panelE = new JPanel();
		panelE.setLayout(new GridLayout(4,3,7,10));
		
		// <<< BUTTON PANEL >>>
		JPanel panelS = new JPanel();
		panelS.setLayout(new GridLayout(2,1,3,5));
		
		JButton c = new JButton();
		c = new JButton("C");
		panelN.add(c);
		c.addActionListener(new buttonListener());

		
		// Adds buttons 1 - 9
		JButton b = new JButton();
		for(int i = 9 ; i > 0; i--)
		{
			b = new JButton(""+i);
			panelB.add(b);
			b.addActionListener(new buttonListener());
		}
		// Adds Zero
		JButton Zero = new JButton("0");
		Zero.setBounds(8, 3, 8, 3);
		panelS.add(Zero);
		Zero.addActionListener(new buttonListener());
		// Adds Operator buttons
		JButton Multi = new JButton("X");
		panelE.add(Multi);
		Multi.addActionListener(new buttonListener());
		JButton Div = new JButton("/");
		panelE.add(Div);
		Div.addActionListener(new buttonListener());
		JButton Min = new JButton("-");
		panelE.add(Min);
		Min.addActionListener(new buttonListener());
		JButton Plus = new JButton("+");
		panelE.add(Plus);
		Plus.addActionListener(new buttonListener());
		JButton Equal = new JButton("=");
		panelE.add(Equal);
		Equal.addActionListener(new buttonListener());
		
		add(panelB, BorderLayout.CENTER);
		add(panelE, BorderLayout.EAST);
		add(panelS, BorderLayout.SOUTH);
		panelS.setVisible(true);
		panelB.setVisible(true);
		panelE.setVisible(true);
		panelE.setPreferredSize(new Dimension(100, 50));
		panelS.setPreferredSize(new Dimension(10, 100));
		panelB.setPreferredSize(new Dimension(100, 50));

	}

	
	
	//Close Window
	class WindowCloser extends WindowAdapter 
  	{
    	public void windowClosing(WindowEvent evt) 
    	{
      		System.exit(0);
    	}
  	}
	class buttonListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent evt)
		{
			String buttonLabel = evt.getActionCommand();
			
	        switch (buttonLabel) 
	        {
            case "0":  storage.add("0");
            		 output.setText(""+storage);
                     break;
            case "1":  storage.add("1");
   		 			 output.setText(""+storage);
                     break;
            case "2":  storage.add("2");
	 			 		output.setText(""+storage);
	 			 		break;
            case "3":  storage.add("3");
	 			 		output.setText(""+storage);
	 			 		break;
            case "4":  storage.add("4");
            			output.setText(""+storage);
            			break;
            case "5":  storage.add("5");
	 			 		output.setText(""+storage);
	 			 		break;
            case "6":  storage.add("6");
	 			 		output.setText(""+storage);
	 			 		break;
            case "7":  storage.add("7");
            			output.setText(""+storage);
	 			 		break;
            case "8":  storage.add("8");
	 			 		output.setText(""+storage);
	 			 		break;
            case "9": storage.add("9");
            		 output.setText(""+storage);
                     break;
            case "C": storage.clear(); 
            		 output.setText(" ");
            		 break;
            case "+": storage.add("+");
            			output.setText(""+storage);
            			break;
            case "-": storage.add("-");
						output.setText(""+storage);
						break;
            case "X": storage.add("*");
						output.setText(""+storage);
						break;
            case "/": storage.add("/");
						output.setText(""+storage);
						break;
            case "=": output.setText(getAnswer(storage) + "");
            			break;
            			
	        }
		    	
			
		}
		
		//getAnswer Method
		public double getAnswer(LinkedList<String> list)
		{
			String[] array = list.toArray(new String[list.size()]);
			double answer = 0.0;
			double sum1  = 0.0;
			
			//while(list.size() > 1)
			//{
				// if operator is true
				if(isOperator(list.get(0)))
				{
					sum1 = getAnswer(list);
					
				}
			//}
			
			return sum1;

		}
		
		//IsOperator Method
		public boolean isOperator(String operand)
		{
			boolean isOperand = false;
			
	        switch (operand) 
	        {
            case "+": isOperand = true;
            break;
            case "-": isOperand = true;
            break;
            case "*": isOperand = true;
            break;
            case "/": isOperand = true;
            break;
	        
	        }
			return isOperand;
		}
		
		//Sum Method
		
		public double sum(double num1, double num2, String operand)
		{
			double answer = 0.0;
			
			switch (operand) 
	        {
            case "+": answer = num1 + num2;
            break;
            case "-": answer = num1 - num2;
            break;
            case "*": answer = num1 * num2;
            break;
            case "/": answer = num1 / num2;
            break;
	        
	        }

			
			return answer;
		}
		
		//getNumber Method
		
		public double getNumber(String [] array)
		{
			
			double number;
			String tmpString = "";
			int count = 0;
			while(array.length > 1 || isOperator(array[count]) == false)
			{
				tmpString = array[count];
				count++;
			}
			
			number = Double.parseDouble(tmpString);
			
			return number;
		}
		public void leftShift(String [] array)
		{
				
				for (int i = 1; i < array.length-1; i++)
				{
					array[i-1] = array[i];
				}
		}
	}
}
