package test;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

public class HelloWorld
{
    public static void main(String [] args)
    {
        Map<Integer ,Integer> map = new HashMap<Integer ,Integer>();
        Scanner in = new Scanner(System.in);
        int largest = 0;
        
        while(in.hasNext())
        {
            String word = in.next();
            int letters = word.length();
           
           if(word.equals("-1"))
           {
        	   in.close();
        	   break;
           }   
            
           if(letters > largest)
           {
        	   largest = letters;
           }
           if(map.containsKey(letters) == true)
           {
               map.put(letters, map.get(letters) + 1);
           }
           
           else
            map.put(letters,1);
            
        }
      
        
        for (Integer name: map.keySet()){

            String key =name.toString();
            String value = map.get(name).toString();  
            System.out.println(key + ": " + value);  


} 
        	
    }
}

